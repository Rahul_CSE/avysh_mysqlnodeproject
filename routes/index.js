var express = require('express');
var router = express.Router();

var mysql = require('mysql');

var con = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "password",
  database: "avstudent"
});

con.connect(function(err) {
  if (err) 
  {
    console.log(err)
  }
})
/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index');
});

router.get('/edit',function(req,res,next){
  res.render('edit');
});

router.get('/view',function(req,res,next){
  res.render('view');
});

router.post('/insertval',(req,res,next)=>{
  a=req.body.st_name
  b=req.body.st_USN
  c=req.body.st_email
  d=req.body.st_phno
  e=req.body.st_sem
  var sql=`Insert into studentinfo(name,usn,email,phonenum,sem) values('${a}','${b}','${c}',${d},${e})`;
        con.query(sql,function (err, result) {
        if (err) {
        throw (err);
        console.log(err)
        }
        console.log("1 record inserted");
    });
  return res.redirect('/');
});

router.post('/getusval',(req,res,next)=>{
  a=req.body.st_name
  var sql=`select * from studentinfo where usn='${a}'`
  con.query(sql,function(err,result,fields){
    if(err) throw err;
    res.render('edits',{
      users:result[0]
    });
  })
});

router.post('/updateval',(req,res,next)=>{
  a=req.body.st_name
  b=req.body.st_USN
  c=req.body.st_email
  d=req.body.st_phno
  e=req.body.st_sem
  var sql=`update studentinfo set name='${a}',email='${c}',phonenum=${d},sem=${e} where usn='${b}'`
  con.query(sql,function(err,result,fields){
    if(err) throw err;
    else res.render('index');
})
});

router.post('/viewusval',(req,res,next)=>{
  a=req.body.st_name
  var sql=`select * from studentinfo where usn='${a}'`
  con.query(sql,function(err,result,fields){
    if(err) throw err;
    res.render('vies',{
      users:result[0]
    });
  })
});
module.exports = router;
